# Eureka Proxy

Declare in local eureka all services of another eureka with http proxies to each service.

# Usage

## Installing
```
npm install -g eureka-synchronizer
```

## Start
```
eureka-synchronizer --help
```

```
Synchronize service available on a distant eureka or kubernetes with the local eureka by creating http proxies to distant services

USAGE
  $ nestjs-console-oclif TARGET

ARGUMENTS
  TARGET  Must be the eureka url or in kubernetes mode the namespace where search services

OPTIONS
  -f, --filter=filter              Allow to filter, with a regexp, the distant services to bind.
  -h, --help                       show CLI help
  -i, --interval=interval          [default: 60] The interval to refresh proxies in seconds.
  -k, --kubernetes                 Enable kubernetes mode. Search services in kubernetes cluster and inject them in eureka.
  -p, --port=port                  [default: 9000] The first port to bind proxies.
  -s, --subDomain=subDomain        Add a sub-domain to each host in eureka. May be useful by the proxy to resolve distant services.
  -x, --proxy=proxy                The proxy to use to contact the eureka.
  --replaceHost=replaceHost        [default: ] Replace value in each host in eureka. It use the sed pattern : /value to replace/final value/
  --replaceService=replaceService  [default: ] Replace value in service name. It use the sed pattern : /value to replace/final value/

EXAMPLES
  $ eureka-synchronizer --help
  $ eureka-synchronizer http://my-distant.eureka.com/eureka/apps
  $ eureka-synchronizer --kubernetes myNamespace
  $ eureka-synchronizer http://my-distant.eureka.com/eureka/apps --replaceHost /.svc/.priv.myCompagny.com/
  $ eureka-synchronizer --kubernetes default --replaceService /-service// --replaceService /-node/-module/
```

### Kubernetes

`eureka-synchronizer` works with kubernetes and imports kubernetes services into your local eureka.

It use `kubectl` command. Check that your kubectl is configured by running : `kubectl cluster-info`

If the command does not work, follow [kubernetes documentation](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

`eureka-synchronizer` will find services in a namespace of the kubernetes cluster. If you want inject services of the `default` namesapce, run the comamnd : `eureka-synchronizer --kubernetes default`

:warning: You need to have the RBAC permissions to make requests to services via [kubectl proxy](https://kubernetes.io/docs/tasks/access-kubernetes-api/http-proxy-access-api/)

### Example

#### Example with a proxy
```
eureka-synchronizer --proxy http://proxy.corporate.com:3128 http://eureka.dev.net/eureka/apps
```

#### Example with interval, port and filter (name which start with `CORP_SERV_`)
```
eureka-synchronizer -i 30 -p 9000 -f "^CORP_SERV_" http://eureka.dev.net/eureka/apps
```

#### Example with kubernetes cluster and mapping of service names
```
eureka-synchronizer --kubernetes default --replaceService /-service// --replaceService /-node/-module/
```

This command will search services defined in the namespace `default` and map the services name by removing `-service` string and replace in the services name `-node` by `-module`.