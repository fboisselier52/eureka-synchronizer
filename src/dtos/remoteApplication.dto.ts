export interface RemoteApplication {
  name: string;
  host: string;
  port: number | string;
}
