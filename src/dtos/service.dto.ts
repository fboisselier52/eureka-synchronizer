import { Eureka } from 'eureka-js-client';
import * as httpProxy from 'http-proxy';

export interface Service {
  name: string;
  port: number;
  httpProxy?: httpProxy;
  eurekaClient?: Eureka;
}
