import { Injectable, Logger, Optional, Inject } from '@nestjs/common';
import * as HttpProxy from 'http-proxy';
import { OclifFlags } from 'nestjs-console-oclif';
import * as HttpsProxyAgent from 'https-proxy-agent';
import { ClientRequest, IncomingMessage, ServerResponse } from 'http';
import { remoteApplicationsServiceProviderName } from '../remote-applications/remote-applications.provider';
import { RemoteApplicationsService } from '../remote-applications/remote-applications.service';
import { RemoteApplication } from '../dtos/remoteApplication.dto';

@Injectable()
export class ProxyService {
  protected logger: Logger = new Logger(ProxyService.name);

  constructor(
    @Inject(remoteApplicationsServiceProviderName)
    protected readonly remoteApplicationsService: RemoteApplicationsService,
    @OclifFlags('proxy') @Optional() protected readonly proxy: string,
  ) {
    this.logger.debug('Create ProxyService');
  }

  async create(port: number, target: RemoteApplication): Promise<HttpProxy> {
    this.logger.log(`Creating proxy for app ${target.name} on port ${port}`);

    const proxyServer: HttpProxy.ServerOptions = this.getProxyServerOptions(target);

    const httpProxy: HttpProxy = HttpProxy.createProxyServer(proxyServer);

    httpProxy.on('proxyReq', (proxyReq: ClientRequest, req: IncomingMessage) =>
      this.logger.log(`${target.name} > Start request : ${req.method} ${req.url}`),
    );

    httpProxy.on('proxyRes', (proxyRes: IncomingMessage) =>
      this.logger.log(`${target.name} > Response ${proxyRes.statusCode}`),
    );

    httpProxy.on('error', (err: Error, req: IncomingMessage, res: ServerResponse) => {
      this.logger.error(`${target.name} > Error ${err.message}`);
      res.writeHead(500, { 'Content-Type': 'text/plain' });
      res.end(`${err.message}`);
    });

    httpProxy.listen(port);

    this.logger.log(`Proxy created : localhost:${port} -> ${proxyServer.target}`);
    return httpProxy;
  }

  async stop(httpProxy: HttpProxy): Promise<void> {
    await new Promise(resolve => httpProxy.close(resolve));
  }

  private getProxyServerOptions(remote: RemoteApplication): HttpProxy.ServerOptions {
    const target = this.remoteApplicationsService.getProxyTarget(remote);

    const proxyServer: HttpProxy.ServerOptions = { target };

    if (this.proxy) {
      proxyServer.agent = new HttpsProxyAgent(this.proxy);
    }

    return proxyServer;
  }
}
