import { Input, help, integer, string, boolean } from '@oclif/command/lib/flags';
import { IArg } from '@oclif/parser/lib/args';

import { BaseCommand } from 'nestjs-console-oclif';

export class Synchronize extends BaseCommand {
  static description: string =
    'Synchronize service available on a distant eureka or kubernetes with the local eureka ' +
    'by creating http proxies to distant services';

  static examples: string[] = [
    '$ eureka-synchronizer --help',
    '$ eureka-synchronizer http://my-distant.eureka.com/eureka/apps',
    '$ eureka-synchronizer --kubernetes myNamespace',
    '$ eureka-synchronizer http://my-distant.eureka.com/eureka/apps --replaceHost /.svc/.priv.myCompagny.com/',
    '$ eureka-synchronizer --kubernetes default --replaceService /-service// --replaceService /-node/-module/',
  ];

  static flags: Input<any> = {
    ...BaseCommand.flags,
    help: help({ char: 'h' }),
    interval: integer({
      char: 'i',
      description: 'The interval to refresh proxies in seconds.',
      default: 60,
    }),
    proxy: string({
      char: 'x',
      description: 'The proxy to use to contact the eureka.',
    }),
    port: integer({
      char: 'p',
      description: 'The first port to bind proxies.',
      default: 9000,
    }),
    filter: string({
      char: 'f',
      description: 'Allow to filter, with a regexp, the distant services to bind.',
    }),
    subDomain: string({
      char: 's',
      description:
        'Add a sub-domain to each host in eureka. May be useful by the proxy to resolve distant services.',
    }),
    kubernetes: boolean({
      char: 'k',
      description:
        'Enable kubernetes mode. Search services in kubernetes cluster and inject them in eureka.',
      default: false,
    }),
    replaceHost: string({
      description:
        'Replace value in each host in eureka. It use the sed pattern : /value to replace/final value/',
      multiple: true,
      default: [],
    }),
    replaceService: string({
      description:
        'Replace value in service name. It use the sed pattern : /value to replace/final value/',
      multiple: true,
      default: [],
    }),
  };

  static args: IArg[] = [
    {
      name: 'target',
      required: true,
      description:
        'Must be the eureka url or in kubernetes mode the namespace where search services',
    },
  ];
}
