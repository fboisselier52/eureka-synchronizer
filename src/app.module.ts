import { Module, HttpModule } from '@nestjs/common';
import { OclifModule } from 'nestjs-console-oclif';
import { ScheduleModule } from 'nest-schedule';
import { AppService } from './app.service';
import { SynchronizerService } from './synchronizer/synchronizer.service';
import { ProxyService } from './proxy/proxy.service';
import { EurekaRepository } from './eureka/eureka.repository';
import { EurekaService } from './eureka/eureka.service';
import { remoteApplicationsServiceProvider } from './remote-applications/remote-applications.provider';

@Module({
  imports: [
    HttpModule,
    OclifModule.forRoot({ provideFlags: ['kubernetes'] }),
    ScheduleModule.register(),
  ],
  providers: [
    AppService,
    EurekaService,
    EurekaRepository,
    SynchronizerService,
    ProxyService,
    remoteApplicationsServiceProvider,
  ],
})
export class AppModule {}
