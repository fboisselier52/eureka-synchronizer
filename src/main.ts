#!/usr/bin/env node

import { bootstrap } from 'nestjs-console-oclif';
import { Synchronize } from './commands/synchronize';
import { AppModule } from './app.module';

bootstrap({
  nestModule: AppModule,
  singleCommand: Synchronize,
  enableShutdownHooks: true,
});
