import { Injectable, Logger, OnApplicationShutdown } from '@nestjs/common';
import { OclifFlags, OclifCommand } from 'nestjs-console-oclif';
import { InjectSchedule, Schedule } from 'nest-schedule';
import { SynchronizerService } from './synchronizer/synchronizer.service';

const SYNCHRONIZE_CMD = 'synchronizeCmd';

@Injectable()
export class AppService implements OnApplicationShutdown {
  protected logger: Logger = new Logger(AppService.name);
  protected running = false;

  constructor(
    protected synchronizerService: SynchronizerService,
    @OclifFlags('interval') protected interval: number,
    @InjectSchedule() protected readonly schedule: Schedule,
  ) {
    this.logger.debug('Instantiate AppService');
  }

  @OclifCommand('synchronize')
  async run(): Promise<void> {
    this.schedule.scheduleIntervalJob(
      SYNCHRONIZE_CMD,
      this.interval * 1000,
      this.synchronizerService.synchronize.bind(this.synchronizerService),
      {
        waiting: true,
        immediate: true,
      },
    );
    this.running = true;
    // Keep the cmd running
    while (true) {
      await new Promise(resolve => setTimeout(resolve, 3600 * 1000));
    }
  }

  async onApplicationShutdown() {
    if (this.running) {
      this.schedule.cancelJob(SYNCHRONIZE_CMD);
      await this.synchronizerService.stopSynchonization();
    }
  }

}
