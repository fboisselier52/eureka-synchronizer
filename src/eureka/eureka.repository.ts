import { Injectable, HttpService, Logger, Optional } from '@nestjs/common';
import { AxiosProxyConfig, AxiosResponse } from 'axios';
import { OclifFlags } from 'nestjs-console-oclif';
import * as url from 'url';
import { EurekaClient } from 'eureka-js-client';

@Injectable()
export class EurekaRepository {
  private logger = new Logger(EurekaRepository.name);

  constructor(
    protected readonly httpService: HttpService,
    @OclifFlags('proxy') @Optional() protected readonly proxy: string,
  ) {
    this.logger.debug('Instanciate EurekaRepository');
  }

  async getApplications(eurekaUrl: string, useProxy: boolean = false): Promise<Application[]> {
    let proxyConf: AxiosProxyConfig | false = false;
    if (this.proxy && useProxy) {
      const proxyUrl: url.UrlWithStringQuery = url.parse(this.proxy);
      proxyConf = {
        host: proxyUrl.hostname,
        auth: {
          username: proxyUrl.auth ? proxyUrl.auth.split(':')[0] : '',
          password: proxyUrl.auth ? proxyUrl.auth.split(':')[1] : '',
        },
        port: parseInt(proxyUrl.port, 10),
        protocol: proxyUrl.protocol,
      };
    }

    const response: AxiosResponse<ApplicationsWrapper> = await this.httpService
      .get(eurekaUrl, {
        proxy: proxyConf,
        headers: {
          Accept: 'application/json',
        },
      })
      .toPromise();

    return response.data.applications.application;
  }
}

interface ApplicationsWrapper {
  applications: Applications;
}

interface Applications {
  application: Application[];
}

export interface Application {
  name: string;
  instance: EurekaClient.EurekaInstanceConfig[];
}
