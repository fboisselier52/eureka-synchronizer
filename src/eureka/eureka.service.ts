import { Injectable, Logger } from '@nestjs/common';
import { Eureka } from 'eureka-js-client';
import * as _ from 'underscore';
import { address } from 'ip';
import { EurekaRepository, Application } from './eureka.repository';

const myIp: string = address();
const LOCAL_EUREKA_URL: string = 'http://127.0.0.1:8761/eureka/apps';

@Injectable()
export class EurekaService {
  protected logger: Logger = new Logger(EurekaService.name);

  constructor(protected readonly eurekaRepository: EurekaRepository) {
    this.logger.debug('Instantiate EurekaService');
  }

  // Service must provide a name and a port
  async registerLocalApplication(name: string, port: number): Promise<Eureka> {
    const client: Eureka = new Eureka({
      instance: {
        instanceId: `eureka-synschronizer:${name}`,
        app: name,
        hostName: myIp,
        ipAddr: myIp,
        port: {
          // prettier-ignore
          '$': port,
          '@enabled': true,
        },
        vipAddress: name.toLowerCase(),
        healthCheckUrl: `http://${myIp}:${port}/health`,
        homePageUrl: `http://${myIp}:${port}/`,
        statusPageUrl: `http://${myIp}:${port}/info`,
        dataCenterInfo: {
          // prettier-ignore
          'name': 'MyOwn',
          '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
        },
        metadata: {
          isLocalProxy: 'true',
        },
      },
      eureka: {
        // eureka server host / port
        host: '127.0.0.1',
        port: 8761,
        servicePath: '/eureka/apps/',
      },
    });
    await new Promise(resolve => client.start(resolve));
    return client;
  }

  async getLocalApplicationWithoutProxies(): Promise<Application[]> {
    const localApplications = await this.eurekaRepository.getApplications(LOCAL_EUREKA_URL);

    // Keep applications that have at least one server that run locally
    return _.filter(localApplications, (app: Application) => {
      // Keep instances that are not local proxy
      const instances = _.filter(app.instance, instance => !instance!.metadata!.isLocalProxy);
      return instances.length > 0;
    });
  }

  async stopEurekaClient(eurekaClient: Eureka): Promise<void> {
    await new Promise(resolve => eurekaClient.stop(resolve));
  }
}
