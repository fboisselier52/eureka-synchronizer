export async function asyncForEach<T>(
  array: T[],
  iteratee: (value: T, index: number, array: T[]) => Promise<void>,
) {
  for (let index: number = 0; index < array.length; index += 1) {
    await iteratee(array[index], index, array);
  }
}
