import { Injectable, Logger, Optional, Inject } from '@nestjs/common';
import * as _ from 'underscore';
import { OclifFlags } from 'nestjs-console-oclif';
import { RemoteApplication } from '../dtos/remoteApplication.dto';
import { asyncForEach } from '../app.helpers';
import { Service } from '../dtos/service.dto';
import { ProxyService } from '../proxy/proxy.service';
import { EurekaService } from '../eureka/eureka.service';
import { RemoteApplicationsService } from '../remote-applications/remote-applications.service';
import { remoteApplicationsServiceProviderName } from '../remote-applications/remote-applications.provider';

@Injectable()
export class SynchronizerService {
  protected logger: Logger = new Logger(SynchronizerService.name);

  protected nextPortAvailable: number;

  protected proxies: Service[] = [];

  constructor(
    protected readonly eurekaService: EurekaService,
    @Inject(remoteApplicationsServiceProviderName)
    protected readonly remoteApplicationsService: RemoteApplicationsService,
    protected readonly proxyService: ProxyService,
    @OclifFlags('filter') @Optional() protected readonly filter: string,
    @OclifFlags('port') protected readonly port: number,
  ) {
    this.logger.debug('Instantiate SynchronizerService');
    this.nextPortAvailable = port;
  }

  async synchronize(): Promise<void> {
    this.logger.log(`\n\n    - Refresh ${new Date()} -\n\n`);

    const startedApps = await this.eurekaService.getLocalApplicationWithoutProxies();
    const startedAppNames = startedApps.map(app => app.name);
    this.logger.log(
      `Find ${startedAppNames.length} started applications in local : ${startedAppNames}`,
    );

    const remoteApps = await this.getRemoteApps();
    const remoteAppNames = remoteApps.map(app => app.name);
    this.logger.log(`Find ${remoteAppNames.length} remote applications : ${remoteAppNames}`);

    const alreadyStarted = this.proxies.map(app => app.name);
    this.logger.log(`Find ${alreadyStarted.length} proxies alreadyStarted : ${alreadyStarted}`);

    const appNamesToProxy = _.difference(remoteAppNames, _.union(startedAppNames, alreadyStarted));
    this.logger.log(`Find ${appNamesToProxy.length} applications to proxy : ${appNamesToProxy}`);

    // Applications that disappeared on distant eureka
    // And applications started locally and that have a Proxy
    const appNamesToStop = _.union(
      _.difference(alreadyStarted, remoteAppNames),
      _.intersection(alreadyStarted, startedAppNames),
    );
    this.logger.log(`Find ${appNamesToStop.length} proxies to stop : ${appNamesToStop}`);

    const appsToProxy = _.filter(remoteApps, app => _.indexOf(appNamesToProxy, app.name) !== -1);

    await asyncForEach(appsToProxy, async app => await this.start(app));

    const proxiesToStop = _.filter(this.proxies, s => _.indexOf(appNamesToStop, s.name) !== -1);

    await asyncForEach(proxiesToStop, async app => await this.stop(app));
    this.removeProxies(appNamesToStop);
  }

  async stopSynchonization(): Promise<void> {
    await asyncForEach(this.proxies, async app => await this.stop(app));
    this.removeProxies();
  }

  private async start(app: RemoteApplication) {
    const service: Service = {
      name: app.name,
      port: this.nextPortAvailable,
    };
    this.logger.log(`Starting ${service.name} on port ${service.port}`);
    this.nextPortAvailable += 1;

    // Create proxy
    service.httpProxy = await this.proxyService.create(service.port, app);

    // Register proxy in local eureka
    this.logger.log(`Registering service ${service.name} in local eureka`);

    service.eurekaClient = await this.eurekaService.registerLocalApplication(
      service.name,
      service.port,
    );

    // Store Proxy
    this.proxies.push(service);
  }

  private async stop(app: Service) {
    // Stop eureka client
    if (app.eurekaClient) {
      this.logger.log(`Stopping eureka client of ${app.name}`);
      await this.eurekaService.stopEurekaClient(app.eurekaClient);
      delete app.eurekaClient;
    }

    // Stop proxy
    if (app.httpProxy) {
      this.logger.log(`Stopping http proxy of ${app.name}`);
      await this.proxyService.stop(app.httpProxy);
      delete app.httpProxy;
    }
  }

  private removeProxies(names?: string[]) {
    if (names) {
      names.forEach(name => {
        // Remove of storage
        const index = _.indexOf(_.map(this.proxies, 'name'), name);
        if (index !== -1) {
          this.logger.log(`Remove proxy of service ${name}`);
          this.proxies.splice(index, 1);
        } else {
          this.logger.error(`Cannot find proxy ${name}`);
        }
      });
    } else {
      this.logger.log(`Remove all proxies : ${_.map(this.proxies, 'name')}`);
      this.proxies = [];
    }
  }

  private async getRemoteApps(): Promise<RemoteApplication[]> {
    let remoteApps = await this.remoteApplicationsService.getRemoteApplications();

    if (this.filter) {
      const regexp = new RegExp(this.filter, 'i');
      remoteApps = _.filter(remoteApps, app => regexp.test(app.name));
    }
    remoteApps.forEach(app => (app.name = app.name.toUpperCase()));
    return remoteApps;
  }
}
