import { RemoteApplication } from '../dtos/remoteApplication.dto';
import { Logger } from '@nestjs/common';

export abstract class RemoteApplicationsService {
  private static readonly REPLACE_REGEXP = new RegExp('^/([a-zA-Z0-9-.]+)/([a-zA-Z0-9-.]*)/$');

  protected logger: Logger = new Logger(this.constructor.name);
  protected replaceServiceMap: { [name: string]: string } = {};
  protected replaceHostMap: { [name: string]: string } = {};

  constructor(replaceHost: string[], replaceService: string[]) {
    this.logger.debug('Parsing replaceService flags');
    this.replaceServiceMap = this.parseReplaceExpressions(replaceService);

    this.logger.debug('Parsing replaceHost flags');
    this.replaceHostMap = this.parseReplaceExpressions(replaceHost);
  }

  protected parseReplaceExpressions(expressions: string[]): { [name: string]: string } {
    const result: { [name: string]: string } = {};
    if (expressions && expressions.length > 0) {
      expressions.forEach(r => {
        const groups = r.match(RemoteApplicationsService.REPLACE_REGEXP);
        if (!groups || groups.length !== 3) {
          throw new Error(
            `Error when parsing replace expression : ${r}. Expect value as : /replaceMe/newValue/`,
          );
        }
        result[groups[1]] = groups[2];
        this.logger.debug(`Register replace expression : ${groups[1]} => ${groups[2]}`);
      });
    }
    return result;
  }

  private applyReplaceMap(originalValue: string, replaceMap: { [name: string]: string }): string {
    let finalValue = originalValue;
    const replaceKeys = Object.keys(replaceMap);
    if (replaceKeys.length > 0) {
      replaceKeys.forEach(searchValue => {
        finalValue = finalValue.replace(searchValue, replaceMap[searchValue]);
      });
    }
    return finalValue;
  }

  protected getFinalServiceName(serviceName: string): string {
    return this.applyReplaceMap(serviceName, this.replaceServiceMap);
  }

  protected getFinalHostname(hostname: string): string {
    return this.applyReplaceMap(hostname, this.replaceHostMap);
  }

  abstract getRemoteApplications(): Promise<RemoteApplication[]>;
  abstract getProxyTarget(remoteApplication: RemoteApplication): string;
}
