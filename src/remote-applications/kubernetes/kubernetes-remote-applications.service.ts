import { Injectable, OnModuleInit, OnApplicationShutdown, Optional } from '@nestjs/common';
import { OclifArgs, OclifFlags } from 'nestjs-console-oclif';
import { KubeConfig, CoreV1Api } from '@kubernetes/client-node';
import { spawn, ChildProcessWithoutNullStreams } from 'child_process';
import * as _ from 'underscore';
import { RemoteApplication } from '../../dtos/remoteApplication.dto';
import { RemoteApplicationsService } from '../remote-applications.service';

@Injectable()
export class KubernetesRemoteApplicationsService extends RemoteApplicationsService
  implements OnModuleInit, OnApplicationShutdown {
  protected k8sApi: CoreV1Api;
  protected kubectlProxy: ChildProcessWithoutNullStreams;

  constructor(
    @OclifArgs('target') protected readonly namespace: string,
    @OclifFlags('replaceService') @Optional() replaceService: string[],
  ) {
    super([], replaceService);
    this.logger.debug('Instantiate KubernetesRemoteApplicationsService');
    const kc = new KubeConfig();
    kc.loadFromDefault();
    this.k8sApi = kc.makeApiClient(CoreV1Api);
  }

  async getRemoteApplications(): Promise<RemoteApplication[]> {
    try {
      const { body } = await this.k8sApi.listNamespacedService(this.namespace);
      const remoteApps = body.items.map(
        (service): RemoteApplication => ({
          name: service.metadata.name,
          host: service.metadata.name,
          port: service.spec.ports[0].name,
        }),
      );
      remoteApps.forEach(app => {
        app.name = this.getFinalServiceName(app.name);
      });
      return remoteApps;
    } catch (error) {
      this.logger.error(`Error when retrieving distant applications : ${error.message}`);
      return [];
    }
  }

  getProxyTarget(remoteApplication: RemoteApplication): string {
    return `http://127.0.0.1:8001/api/v1/namespaces/${this.namespace}/services/${remoteApplication.host}:${remoteApplication.port}/proxy`;
  }

  onModuleInit() {
    this.logger.debug('Starting kubectl proxy');
    try {
      this.kubectlProxy = spawn('kubectl', ['proxy', '--disable-filter=true'], {
        detached: true,
      });
      this.logger.debug('> kubectl proxy started');
    } catch (error) {
      this.logger.error(error.message);
    }
  }

  onApplicationShutdown() {
    if (this.kubectlProxy) {
      this.logger.debug('Stopping kubectl proxy');
      this.kubectlProxy.kill();
      this.logger.debug('> kubectl proxy stopped');
    }
  }
}
