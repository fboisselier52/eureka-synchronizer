import { Injectable, Optional } from '@nestjs/common';
import { EurekaClient } from 'eureka-js-client';
import * as _ from 'underscore';
import { OclifArgs, OclifFlags } from 'nestjs-console-oclif';
import { RemoteApplication } from '../../dtos/remoteApplication.dto';
import { RemoteApplicationsService } from '../remote-applications.service';
import { EurekaRepository } from '../../eureka/eureka.repository';
import { isIP } from 'net';

@Injectable()
export class EurekaRemoteApplicationsService extends RemoteApplicationsService {
  constructor(
    protected readonly eurekaRepository: EurekaRepository,
    @OclifArgs('target') protected readonly distantEureka: string,
    @OclifFlags('subDomain') @Optional() protected readonly subDomain: string,
    @OclifFlags('replaceHost') @Optional() replaceHost: string[],
    @OclifFlags('replaceService') @Optional() replaceService: string[],
  ) {
    super(replaceHost, replaceService);
  }

  async getRemoteApplications(): Promise<RemoteApplication[]> {
    const eurekaApplications = await this.eurekaRepository.getApplications(
      this.distantEureka,
      true,
    );
    const remoteApps = eurekaApplications.map(app => ({
      name: app.name,
      host: app.instance[0].hostName,
      port: (app.instance[0].port as EurekaClient.LegacyPortWrapper).$,
    }));

    remoteApps.forEach(app => {
      app.name = this.getFinalServiceName(app.name);
    });
    return remoteApps;
  }

  getProxyTarget(remoteApplication: RemoteApplication): string {
    let finalHostname = remoteApplication.host;
    if (isIP(finalHostname) === 0) {
      // If it is not an IP
      if (this.subDomain && !finalHostname.endsWith(this.subDomain)) {
        finalHostname += `.${this.subDomain}`;
      }

      finalHostname = this.getFinalHostname(finalHostname);
    }

    return `http://${finalHostname}:${remoteApplication.port}`;
  }
}
