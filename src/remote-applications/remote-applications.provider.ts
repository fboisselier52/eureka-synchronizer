import { Provider } from '@nestjs/common';
import { EurekaRepository } from '../eureka/eureka.repository';
import { KubernetesRemoteApplicationsService } from './kubernetes/kubernetes-remote-applications.service';
import { EurekaRemoteApplicationsService } from './eureka/eureka-remote-applications.service';

export const remoteApplicationsServiceProviderName: string = 'RemoteApplicationsService';

export const remoteApplicationsServiceProvider: Provider = {
  provide: remoteApplicationsServiceProviderName,
  useFactory: (
    kubernetes: boolean,
    target: string,
    subDomain: string,
    eurekaRepository: EurekaRepository,
    replaceHost: string[],
    replaceService: string[],
  ) => {
    if (kubernetes) {
      return new KubernetesRemoteApplicationsService(target, replaceService);
    } else {
      return new EurekaRemoteApplicationsService(
        eurekaRepository,
        target,
        subDomain,
        replaceHost,
        replaceService,
      );
    }
  },
  inject: [
    'oclif.flags.kubernetes',
    'oclif.args.target',
    'oclif.flags.subDomain',
    EurekaRepository,
    'oclif.flags.replaceHost',
    'oclif.flags.replaceService',
  ],
};
